# SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# Due to performance limitations, add wait-online helper
IMAGE_INSTALL:append:cassini = " wait-online"

# Since the host has two stage booting, we need to ensure we
# only get a single ip address for the same MAC incase of reboot
IMAGE_INSTALL:append:cassini = " send-no-hostname"

# Mender configuration
INHERIT:remove:firmware = " ${@bb.utils.contains('VIRTUAL-RUNTIME_ota_update',\
        'mender-ota', 'mender-maybe-setup', '',d)}"
MENDER_EFI_LOADER:firmware = ""
FIRMWARE_BINARIES:remove:pn-firmware-deploy-image = " ${@bb.utils.contains(\
    'VIRTUAL-RUNTIME_ota_update', 'mender-ota', \
    ' corstone1000_capsule_cert.crt corstone1000_capsule_key.key', '',d)}"

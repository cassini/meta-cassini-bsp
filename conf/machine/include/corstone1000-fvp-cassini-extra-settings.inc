# SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

include conf/machine/include/corstone1000-mps3-cassini-extra-settings.inc

# FVP parameters
# Setup the second MMC card
FVP_CONFIG[board.msd_mmc_2.diagnostics] ?= "0"

IMAGE_EXTENSION = "${@bb.utils.contains('VIRTUAL-RUNTIME_ota_update', \
                            'mender-ota', \
                            'uefiimg', 'wic', d)}"

FVP_CONFIG[board.msd_mmc.p_mmc_file] = "${IMAGE_NAME}.${IMAGE_EXTENSION}"

<!--
# SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT
-->

# Cassini BSP

## Introduction

This Yocto layer is used by
[meta-cassini](https://gitlab.com/Linaro/cassini/meta-cassini).

It modifies some Arm-maintained machine definitions and recipes from
[meta-arm-bsp](https://git.yoctoproject.org/meta-arm/tree/meta-arm-bsp) so that
they can be used to build and boot Cassini distro images.

Currently supported platforms include:
* Corstone-1000 FVP (corstone1000-fvp)
* Corstone-1000 for MPS3 (corstone1000-mps3)

At any given point in time, this repository may hold recipes, append-files,
config fragments, and/or out-of-tree patches which either:
- have not been upstreamed yet
- cannot be upstreamed (they are Cassini specific or otherwise inappropriate)

For more details, see the Cassini documentation
[here](https://cassini.readthedocs.io/en/latest/index.html)

Contributions to this repository are not accepted at this time

## Repository License

The repository's standard license is the MIT license, under which most of the
repository's content is provided. Exceptions to this standard license relate to
files that represent modifications to externally licensed works (for example,
patch files). These files may therefore be included in the repository under
alternative licenses in order to be compliant with the licensing requirements of
the associated external works.

License details may be found in the [local license file](LICENSE.rst), or as
part of the project documentation.

## Reporting Issues

Please report problems using GitLab's "Issues" feature.

## Reporting Security Issues

If you find any security vulnerabilities, please do not report them via GitLab
Instead, send an email to the security team at psirt@arm.com stating that you
may have found a security vulnerability in meta-cassini-bsp.

## Maintainer(s)

* Adam Johnston <adam.johnston@arm.com>

## Disclaimer

Arm reference solutions are Arm public example software projects that track and
pull upstream components, incorporating their respective security fixes
published over time. Arm partners are responsible for ensuring that the
components they use contain all the required security fixes, if and when they
deploy a product derived from Arm reference solutions.

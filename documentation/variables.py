# SPDX-FileCopyrightText: Copyright (c) 2023-2024, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2022-2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# This file centralizes the variables and links used throughout the
# documentation. The dictionaries are converted to a single string that is used
# as the rst_prolog (see the Sphinx Configuration documentation at
# https://www.sphinx-doc.org/en/master/usage/configuration.html for more info).

# There are two types of key-value substitutions:
#     1. simple string replacements
#     2. replacement with a rendered hyperlink, where the key defines what the
#        rendered hyperlink text will be

# Prepend the key with "link:" to identify it as a Sphinx target name for use
# as a hyperlink. The "link:" prefix is dropped from the substitution name.
#
# For example:
#   "link:This URL": "www.arm.com"
#   "company name": "arm"
# Can be used as:
#   The |company name| website can be found at |This URL|_.
#
# Note the "_" which renders the substitution as a hyperlink is only possible
# because the variable is defined as a link, to be resolved as a Sphinx target.

""" Called from the parent (Cassini) documentation build to initialise
variables which are referenced from documentation sources in this repository
"""

DOCS_RELEASE = "latest"

general_links = {
    "link:Arm Corstone-1000 Technical Overview":
        "https://developer.arm.com/documentation/102360/0000",
    "link:Arm Corstone-1000 Software":
        "https://corstone1000.docs.arm.com/en/"
        f"{DOCS_RELEASE}",
    "link:Fast Models Fixed Virtual Platforms (FVP) Reference Guide":
        "https://developer.arm.com/documentation/100966/1119",
    "link:Fast Models Reference Guide":
        "https://developer.arm.com/documentation/100964/1119/"
        "Introduction-to-Fast-Models/User-mode-networking",
    "link:EULA": "https://developer.arm.com/downloads/"
        "-/arm-ecosystem-fvps/eula",
}


def generate_replacement(key, value):
    """ Generate simple string substitution """

    replacement = f".. |{key}| replace:: {value}"
    return f"{replacement}"


def generate_link(key, link):
    """ Generate link substitution """

    definition = f".. _{key}: {link}"
    key_mapping = f".. |{key}| replace:: {key}"
    return f"{definition}\n{key_mapping}"


def generate_rst_prolog():
    """ Generate all substitutions that should be available in every file """

    rst_prolog = ""

    for variables_group in [general_links]:
        for key, value in variables_group.items():
            if key.startswith("link:"):
                rst_prolog += generate_link(key.split("link:")
                                            [1], value) + "\n"
            else:
                rst_prolog += generate_replacement(key, value) + "\n"

    return rst_prolog

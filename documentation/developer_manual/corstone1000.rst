..
 # SPDX-FileCopyrightText: Copyright (c) 2023, Linaro Limited.
 #
 # SPDX-FileCopyrightText: <text>Copyright 2022-2024 Arm Limited and/or its
 # affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: MIT

Corstone-1000 for MPS3
======================

  * **Corresponding value for** ``MACHINE`` **variable**: ``corstone1000-mps3``
  * **Target Platform Config**: ``kas/corstone1000-mps3.yml``

  To read documentation about the Corstone-1000, see the
  |Arm Corstone-1000 Technical Overview|_.

  For more information about the software stack for the Corstone-1000, see
  |Arm Corstone-1000 Software|_.

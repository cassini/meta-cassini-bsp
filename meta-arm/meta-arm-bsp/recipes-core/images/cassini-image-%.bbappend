# SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# Ensure cassini-image-* also builds the firmware for corstone1000 using a different libc
do_image_complete[depends] = "${@bb.utils.contains_any('MACHINE','corstone1000-mps3 corstone1000-fvp', \
                              'firmware-deploy-image:do_deploy', '', d)}"

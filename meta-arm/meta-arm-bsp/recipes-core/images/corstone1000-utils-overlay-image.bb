# SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

SUMMARY = "Overlay image containing bmaptool"
DESCRIPTION = "Image used during testing of initramfs based platforms to \
               include additional tools used to provision boards being \
               tested"
LICENSE = "MIT"

DEPENDS += "trusted-firmware-a"

inherit image
inherit nopackages

PACKAGE_INSTALL = "bmaptool"

IMAGE_FEATURES = ""
IMAGE_LINGUAS = ""

IMAGE_ROOTFS_SIZE = "0"
IMAGE_ROOTFS_EXTRA_SPACE = "0"

IMAGE_FSTYPES = "tar.bz2"

# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}:"

SRC_URI:append:corstone1000 = " \
    file://timeouts.conf \
"

# nooelint: oelint.func.specific - Common name set in Corstone-1000 definitions
do_install:append:corstone1000() {
    install -D -m0644 ${UNPACKDIR}/timeouts.conf ${D}${systemd_system_unitdir}.conf.d/01-${PN}.conf
}

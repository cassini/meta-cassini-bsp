# SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend:cassini := "${THISDIR}:${THISDIR}/linux-yocto:"

# nooelint: oelint.vars.srcurifile - False positive
SRC_URI:append:cassini:corstone1000 = " \
    file://cgroups.cfg \
    file://container.cfg \
    file://network.cfg \
    ${@bb.utils.contains('DISTRO_FEATURES', \
            'cassini-sdk', \
            'file://sdk.cfg', '', d)} \
    "
